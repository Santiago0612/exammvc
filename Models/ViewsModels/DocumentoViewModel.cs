﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Prueba.Models.ViewsModels
{
    public class DocumentoViewModel
    {
        public string Cedula { get; set; }
        public string Descripcion { get; set; }
        public string Nombre { get; set; }

        public List<Detalle> detalles { get; set; }

    }

    public class Detalle
    {
        public string Codigo { get; set; }
        public string NombreCuenta { get; set; }
        public decimal Debito { get; set; }

        public decimal Credito { get; set; }
    }
}