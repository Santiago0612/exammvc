﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Prueba.Models.ViewsModels;
using Prueba.Models;
namespace Prueba.Controllers
{
    public class MaestroDetalleController : Controller
    {
        // GET: MaestroDetalle
        public ActionResult Add()
        {
           
            return View();
        }

        [HttpPost]
        public ActionResult Add(DocumentoViewModel model)
        {
            try
            {
                using (PruebaEntities db = new PruebaEntities())
                {
                    Documento doc = new Documento();
                    doc.fecha = DateTime.Now;
                    doc.cedula = model.Cedula;
                    doc.descripcion = model.Descripcion;
                    doc.nombre = model.Nombre;
                    db.Documento.Add(doc);
                    db.SaveChanges();

                    foreach (var i in model.detalles)
                    {
                        detalle oDetalle = new detalle();
                        oDetalle.codigo = i.Codigo;
                        oDetalle.nombreCuenta = i.NombreCuenta;
                        oDetalle.debito = i.Debito;
                        oDetalle.credito = i.Credito;
                        oDetalle.idDocumento = doc.id;
                        db.detalle.Add(oDetalle);

                        
                    }
                    db.SaveChanges();


                }
                ViewBag.Message = "Venta registrada correctamente";
                return View();
            }
            catch (Exception ex)
            {
                return View(model);
            }
            

        }
    }
}